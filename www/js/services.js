angular.module('starter.services', [])
//es importante aclarar que cuando se obtengan datos externos, esta informacion debe ser proveida desde services
//dejo comentado un ejemplo de objetos hard coded

//creamos un factory, el primer elemento es el nombre del servicio 'EncuestasService' que podría ser pepito
//y luego en un array una funcion anonima, precesida por los servicios de angular o cualquier elemento que queramos usar
.factory('EncuestasService', ['$http', function($http){
	//variable para conectar al servidor
	var server_prefix='http://127.0.0.1:8000';
	//para guardar las ultimas respuestas votadas almacenamos en un array
	var votes=[];
	//retornamos un objeto de funciones
	return {
		//nos devolvera todos los datos, null es para prever que la comunicacion con el servidor podria fallar
		all:function(callback){
			$http({
				method:'GET',
				url: server_prefix+'/encuestas/api/questions'
			}).then(function(r){
				callback(null,r.data);
			});
		},
		get:function(id, callback){
			$http({
				method:'GET',
				url:server_prefix+'/encuestas/api/questions/'+id
			}).then(function(r){
				callback(null,r.data)
			})
		},//ahora una funcion que contabilize los votos, le pasamos 3 parametros
		vote:function(encuestaID, optionID, callback){
			$http({
				method:'POST',
				url:server_prefix+'/encuestas/api/questions/'+encuestaID+'/vote',
				contentType:'application/json',
				data:{
					choice:optionID
				}
			}).then(function(r){
				if(r.data=='ok'){
					callback(null)
				}else{
					console.log(r);
					callback('Hubo un error inesperado');
				}
			})
		},
		ultimasRespuestas:function(){
			//retornamos el array con votos
			return votes;
		}	
	}
}])