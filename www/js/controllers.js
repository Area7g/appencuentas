angular.module('starter.controllers', [])

.controller('AppCtrl', ['$scope', '$ionicModal', '$timeout', function($scope, $ionicModal, $timeout) {
  
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
}])
//el nombre del controlador lo definimos en app.js
.controller('EncuestasController', ['$scope', 'EncuestasService', function($scope, EncuestasService) {
  //ejecutamos la funcion all de 'EncuestasService', en el cual obtuvimos el objeto con todos sus atributos
  EncuestasService.all(function(err, data){
    //el servicio $scope (alcanze) crea la variable 'encuestas' y en el almacenamos la data que llega
    $scope.encuestas = data;
  });
}])
.controller('EncuestaController', ['$scope', '$stateParams', '$ionicPopup', '$state', 'EncuestasService', function($scope, $stateParams, $ionicPopup, $state, EncuestasService) {
  //para el ng-model declaramos la variable con un objeto que en principio sera null
  $scope.data = {
    choice:null
  }

  //llamamos a la funcion get() que hicimos en services, debemos obtener el id
  //el id va a venir de app.js del state 'encuesta' #/app/encuestas/:id
  //para eso usamos el servicio $stateParams.encuestaID que fue la variable que pusimos en la url
  EncuestasService.get($stateParams.encuestaID, function(err, data){
    //pasamos el contexto a la plantilla con los datos recibidos
    $scope.encuesta = data;
  });
  //ahora haremos la funcion de votar
  $scope.vote = function(){
    //llamamos a la funcion vote() de service
    //y le decimos que va a votar en el id correspondiente como primer parametro
    //como segundo parametro le pasamo la data que enviamos desde la vista 'data.choice'
    //como tercer parametro una funcion con un error posible
    EncuestasService.vote($stateParams.encuestaID, $scope.data.choice, function(err){
      if(err){
        console.log(err);
        return;
      }
      //avisamos al usuario del voto realizado si no hay error
      var alertPopup = $ionicPopup.alert({
        title:'Voto realizado!',
        //podemos pasar incluso una plantill
        template:'Gracias por participar!',
        buttons: [
          {
            text: 'ok',
            type: 'button-assertive'
          }
        ]
      });
      alertPopup.then(function(res){
        //si se realizo un voto lo dirigiremos a la vista de resultados
        $state.go('app.resultado',{
          encuestaID:$stateParams.encuestaID
        }); 
      });
    })
  }
}])
.controller('ResultadoController', ['$scope', '$stateParams', 'EncuestasService', function($scope, $stateParams, EncuestasService) {
  EncuestasService.get($stateParams.encuestaID, function(err, data){
    //el servicio $scope (alcanze) crea la variable 'encuestas' y en el almacenamos la data que llega
    $scope.encuesta = data;
  });
}])
.controller('RespuestasController', ['$scope', 'EncuestasService', function($scope, EncuestasService) {
  $scope.respuestas = EncuestasService.ultimasRespuestas();
}])
